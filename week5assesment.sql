create database travel;
use travel;
create table PASSENGER (Passenger_name varchar(20),   Category               varchar(20),   Gender                 varchar(20),   Boarding_City      varchar(20),   Destination_City   varchar(20),  Distance                int,  Bus_Type             varchar(20));
create table PRICE(             Bus_Type   varchar(20),             Distance    int,              Price      int          );
insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');
select * from passenger;
insert into price values('Sleeper',350,770);insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);insert into price values('Sitting',1500,1860);
select * from price;


select count( Passenger_name) from passenger where distance>=600 group by gender;


select gender,count( Passenger_name) from passenger where distance>=600 group by gender;


select  Bus_Type,min(price) from price where  Bus_Type='sleeper';


select  Passenger_name from passenger where  Passenger_name like 'S%';


select Passenger_name, Boarding_City, Destination_City,ps. Bus_Type, Price from passenger ps,price pr where ps.distance=pr.distance and ps.bus_type=pr.bus_type;


select passenger_name,pr.price from passenger ps,price pr where (ps.distance=1000 and ps.bus_type='sitting')and pr.bus_type=ps.bus_type and pr.distance=ps.distance;


select passenger_name,pr.bus_type,price from passenger ps,price pr where passenger_name='pallavi' group by pr.bus_type;


select  distinct(distance) from passenger order by distance desc;


select passenger_name,distance*100/(select sum(distance) from passenger) as percent from passenger ;



create view view1 as select * from passenger where category='ac';
select * from view1;


delimiter //
create procedure sleep()
begin
select count(passenger_name) from passenger where bus_type='sleeper';
end
//
call sleep();
//
select * from passenger limit 5;
create database week5;
use week5;

create table project ( projectcode varchar(6),projectname varchar(20),projectmanager varchar(20),projectbudget int(10), primary key(projectcode));
desc project;
insert into project values("PC010","Reservation System","Mr.Ajay","120500");
insert into project values("PC011","HR Systems","Ms.Charu",500500);
insert into project values("PC012","Attendance System","Mr.Rajesh",710700);
select * from project;

create table employees(employeeno varchar(4), employeename varchar(10), departmenttno varchar(5),departmenttname varchar(10), hourlyrate decimal(4,2),primary key(employeeno));
desc employees;
insert into employees values("S100","Mohan","D03","Database",21.00);
insert into employees values("S101","vipul","D02","Testing",16.50);
insert into employees values("S102","Riyaz","D01","IT",22.00);
insert into employees values("S103","Pavan","D03","Database",18.50);
insert into employees values("S104","Jitendra","D02","Testing",17.00);
insert into employees values("S315","Pooja","D01","IT",23.50);
insert into employees values("S137","Rahul","D03","Database",21.50);
insert into employees values("S218","Avneesh","D02","Testing",15.50);
insert into employees values("S109","Vikas","D01","IT",20.50);
select * from employees;

create table department(departmentno varchar(4),departmentname varchar(10),primary key(departmentno));
desc department;
 insert into department values("D01","IT");
 insert into department values("D02","Testing");
 insert into department values("D03","Database");
 select * from department;
 
 create table emp(employeeno varchar(4), employeename varchar(10),hourlyrate decimal(4,2),primary key(employeeno));
desc emp;
 insert into emp values("S100","Mohan",21.00);
insert into emp values("S101","vipul",16.50);
insert into emp values("S102","Riyaz",22.00);
insert into emp values("S103","Pavan",18.50);
insert into emp values("S104","Jitendra",17.00);
insert into emp values("S315","Pooja",23.50);
insert into emp values("S137","Rahul",21.50);
insert into emp values("S218","Avneesh",15.50);
insert into emp values("S109","Vikas",20.50);
select * from emp;


create table last(employeeno varchar(4),hourlyrate decimal(4,2),primary key(employeeno));
desc last;
insert into last values("S100",21.00);
insert into last values("S101",16.50);
insert into last values("S102",22.00);
insert into last values("S103",18.50);
insert into last values("S104",17.00);
insert into last values("S315",23.50);
insert into last values("S137",21.50);
insert into last values("S218",15.50);
insert into last values("S109",20.50);
select * from last;



